package cn.edu.dgut.css.sai.security.oauth2.config;

import org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2ClientProperties;
import org.springframework.boot.context.properties.PropertyMapper;
import org.springframework.boot.convert.ApplicationConversionService;
import org.springframework.core.convert.ConversionException;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrations;
import org.springframework.security.oauth2.core.AuthenticationMethod;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 代码源自:{@link org.springframework.security.config.oauth2.client.CommonOAuth2Provider}
 *
 * @author Sai
 * @see org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2ClientPropertiesRegistrationAdapter
 * @since 1.0.5
 */
final class SaiOAuth2ClientPropertiesRegistrationAdapter {

    /* 私有构造方法，防止被实例化 */
    private SaiOAuth2ClientPropertiesRegistrationAdapter() {
    }

    static Map<String, ClientRegistration> getClientRegistrations(
            OAuth2ClientProperties properties) {
        Map<String, ClientRegistration> clientRegistrations = new HashMap<>();
        properties.getRegistration().forEach((key, value) -> clientRegistrations.put(key,
                getClientRegistration(key, value, properties.getProvider())));
        return clientRegistrations;
    }

    private static ClientRegistration getClientRegistration(String registrationId,
                                                            OAuth2ClientProperties.Registration properties,
                                                            Map<String, OAuth2ClientProperties.Provider> providers) {
        ClientRegistration.Builder builder = getBuilderFromIssuerIfPossible(registrationId,
                properties.getProvider(), providers);
        if (builder == null) {
            builder = getBuilder(registrationId, properties.getProvider(), providers);
        }
        PropertyMapper map = PropertyMapper.get().alwaysApplyingWhenNonNull();
        map.from(properties::getClientId).to(builder::clientId);
        map.from(properties::getClientSecret).to(builder::clientSecret);
        map.from(properties::getClientAuthenticationMethod)
                .as(ClientAuthenticationMethod::new)
                .to(builder::clientAuthenticationMethod);
        map.from(properties::getAuthorizationGrantType).as(AuthorizationGrantType::new)
                .to(builder::authorizationGrantType);
        map.from(properties::getRedirectUri).to(builder::redirectUriTemplate);
        map.from(properties::getScope).as(StringUtils::toStringArray)
                .to(builder::scope);
        map.from(properties::getClientName).to(builder::clientName);
        return builder.build();
    }

    private static ClientRegistration.Builder getBuilderFromIssuerIfPossible(String registrationId,
                                                                             String configuredProviderId, Map<String, OAuth2ClientProperties.Provider> providers) {
        String providerId = (configuredProviderId != null) ? configuredProviderId
                : registrationId;
        if (providers.containsKey(providerId)) {
            OAuth2ClientProperties.Provider provider = providers.get(providerId);
            String issuer = provider.getIssuerUri();
            if (issuer != null) {
                String cleanedIssuer = cleanIssuerPath(issuer);
                ClientRegistration.Builder builder = ClientRegistrations
                        .fromOidcIssuerLocation(cleanedIssuer)
                        .registrationId(registrationId);
                return getBuilder(builder, provider);
            }
        }
        return null;
    }

    private static String cleanIssuerPath(String issuer) {
        if (issuer.endsWith("/")) {
            return issuer.substring(0, issuer.length() - 1);
        }
        return issuer;
    }

    private static ClientRegistration.Builder getBuilder(String registrationId, String configuredProviderId,
                                                         Map<String, OAuth2ClientProperties.Provider> providers) {
        String providerId = (configuredProviderId != null) ? configuredProviderId
                : registrationId;
        // 自定义
        SaiCommonOAuth2Provider provider = getCommonProvider(providerId);
        if (provider == null && !providers.containsKey(providerId)) {
            throw new IllegalStateException(
                    getErrorMessage(configuredProviderId, registrationId));
        }
        ClientRegistration.Builder builder = (provider != null) ? provider.getBuilder(registrationId)
                : ClientRegistration.withRegistrationId(registrationId);
        if (providers.containsKey(providerId)) {
            return getBuilder(builder, providers.get(providerId));
        }
        return builder;
    }

    private static String getErrorMessage(String configuredProviderId,
                                          String registrationId) {
        return ((configuredProviderId != null)
                ? "Unknown provider ID '" + configuredProviderId + "'"
                : "Provider ID must be specified for client registration '"
                + registrationId + "'");
    }

    private static ClientRegistration.Builder getBuilder(ClientRegistration.Builder builder, OAuth2ClientProperties.Provider provider) {
        PropertyMapper map = PropertyMapper.get().alwaysApplyingWhenNonNull();
        map.from(provider::getAuthorizationUri).to(builder::authorizationUri);
        map.from(provider::getTokenUri).to(builder::tokenUri);
        map.from(provider::getUserInfoUri).to(builder::userInfoUri);
        map.from(provider::getUserInfoAuthenticationMethod).as(AuthenticationMethod::new)
                .to(builder::userInfoAuthenticationMethod);
        map.from(provider::getJwkSetUri).to(builder::jwkSetUri);
        map.from(provider::getUserNameAttribute).to(builder::userNameAttributeName);
        return builder;
    }

    /**
     * 使用自定义的oauth2厂商参数设置提供器
     *
     * @param providerId 第三登录的id
     * @return {@link SaiCommonOAuth2Provider}
     */
    private static SaiCommonOAuth2Provider getCommonProvider(String providerId) {
        try {
            return ApplicationConversionService.getSharedInstance().convert(providerId,
                    SaiCommonOAuth2Provider.class);
        } catch (ConversionException ex) {
            return null;
        }
    }
}
